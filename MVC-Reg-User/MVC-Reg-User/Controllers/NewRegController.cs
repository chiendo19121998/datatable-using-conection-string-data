﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC_Reg_User.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using Microsoft.Ajax.Utilities;
using System.Data;

namespace MVC_Reg_User.Controllers
{
    public class NewRegController : Controller
    {
        // GET: NewReg
        database_Access_layer.db dblayer = new database_Access_layer.db();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(UserClass uc, HttpPostedFileBase file)
        {
            string mainconn = ConfigurationManager.ConnectionStrings["Myconnection"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(mainconn);
            string sqlquery = "insert into [dbo].[MVCregUser] (Uname,Uemail,Upwd,Gender,Uimg) values(@Uname,@Uemail,@Upwd,@Gender,@Uimg)";
            SqlCommand sqlcomm = new SqlCommand(sqlquery, sqlconn);
            sqlconn.Open();
            sqlcomm.Parameters.AddWithValue("@Uname", uc.Uname);
            sqlcomm.Parameters.AddWithValue("@Uemail", uc.Uemail);
            sqlcomm.Parameters.AddWithValue("@Upwd", uc.Upwd);
            sqlcomm.Parameters.AddWithValue("@Gender", uc.Gender);
            if (file != null && file.ContentLength > 0)
            {
                string filename = Path.GetFileName(file.FileName);
                string imgpath = Path.Combine(Server.MapPath("~/User-Images/"), filename);
                file.SaveAs(imgpath);
            }
            sqlcomm.Parameters.AddWithValue("@Uimg", "~/User-Images/" + file.FileName);
            sqlcomm.ExecuteNonQuery();
            sqlconn.Close();
            ViewData["Message"] = "User Record" + uc.Uname + "Đã Lưu Thành Công !";
            return View();
        }
        public ActionResult Home()
        {

            return View();


        }

        public JsonResult get_data()
        {
            DataSet ds = dblayer.Show_Data();
            List<MVCregUser> list = new List<MVCregUser>();
           
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                list.Add(new MVCregUser
                {
                    ID = Convert.ToInt32(dr["ID"]),
                    Uname=dr["Uname"].ToString(),
                    Uemail = dr["Uemail"].ToString(),
                    Upwd = dr["Upwd"].ToString(),
                    Gender = dr["Gender"].ToString(),
                    Uimg = dr["Uimg"].ToString(),
                });
            }
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            
        }
    }
}

   
            


  

