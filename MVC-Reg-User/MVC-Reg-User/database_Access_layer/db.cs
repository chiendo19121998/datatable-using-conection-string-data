﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace MVC_Reg_User.database_Access_layer
{
    public class db
    {
        string mainconn = ConfigurationManager.ConnectionStrings["Myconnection"].ConnectionString;
        public DataSet Show_Data()
        {
            SqlConnection conn = new SqlConnection(mainconn);
            string sqlquery = "  select * from [dbo].[MVCregUser]";
            SqlCommand cm = new SqlCommand(sqlquery, conn);
            SqlDataAdapter ad = new SqlDataAdapter(cm);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            return ds;
        }
    }

}