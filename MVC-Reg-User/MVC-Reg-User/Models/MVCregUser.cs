﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Reg_User
{
    public class MVCregUser
    {
        public int ID { get; set; }
        public string Uname { get; set; }
        public string Uemail { get; set; }
        public string Upwd { get; set; }
        public string Gender { get; set; }
        public string Uimg { get; set; }
    }
}